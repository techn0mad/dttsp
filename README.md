# README #

DttSP Consolidation project

### What is this repository for? ###

* An attempt to reconverge all of the variants of the [DttSP project](http://dttsp.sourceforge.net/):
    * The original version hosted on [sourceforge](http://dttsp.sourceforge.net/cvs.html)
    * The official version from the [Comprehensive GNU Radio Archive Network (CGRAN)](https://128.2.212.19/browser/projects/dttsp)
    * Multiple copies in the [HPSDR repository](http://svn.tapr.org/repos_sdr_hpsdr/)
    * Some released versions from [FlexRadio](http://kc.flex-radio.com/KnowledgebaseArticle50392.aspx)
    * Possible variants from John Naylor, Andrea Montefusco, Alex Lee

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

#### Date ranges for DttSP repos ####

Repo | Type | Earliest commit | Latest commit
-----|------|-----------------|--------------
[Sourceforge](http://dttsp.cvs.sourceforge.net/viewvc/dttsp/dttsp/) | CVS | Wed Mar 9 16:10:43 2005 +0000 | Wed Jun 22 20:18:43 2005 +0000
[CGRAN](https://www.cgran.org/browser/projects/dttsp) | svn | Sat Oct 25 14:26:02 2008 +0000 | Mon Jun 22 15:18:07 2009 +0000

#### Age of DttSP variants in the [HPSDR repo](http://svn.tapr.org/repos_sdr_hpsdr/) ####

Directory | Age | Revision
----------|-----|---------
[WA8YWQ/ghpsdr3-Windows/DttSP](http://svn.tapr.org/repos_sdr_hpsdr/trunk/WA8YWQ/ghpsdr3-Windows/DttSP/) | 1358d 23h | 1670
[WD5Y/PowerSDR - vs2008/HPSDR @ v1.2.2/Source/DttSP](http://svn.tapr.org/repos_sdr_hpsdr/trunk/WD5Y/PowerSDR%20-%20vs2008/HPSDR%20@%20v1.2.2/Source/DttSP/) | 1304d 23h | 1713
[WD5Y/PowerSDR/Source/DttSP](http://svn.tapr.org/repos_sdr_hpsdr/trunk/WD5Y/PowerSDR/Source/DttSP/) | 1704d 03h | 1145
[AE6VK/HPSDR Sources 03-Jan-2010/PowerSDR-PennyMerge rev 3470/Source/DttSP](http://svn.tapr.org/repos_sdr_hpsdr/trunk/AE6VK/HPSDR%20Sources%2003-Jan-2010/PowerSDR-PennyMerge%20rev%203470/Source/DttSP/) | 1588d 21h | 1274
[W5WC/PowerSDR_HPSDR_2_RX2/Source/DttSP](http://svn.tapr.org/repos_sdr_hpsdr/trunk/W5WC/PowerSDR_HPSDR_2_RX2/Source/DttSP/) | 427d 22h | 2937
[W5WC/PowerSDR_HPSDR_mRX/Source/DttSP](http://svn.tapr.org/repos_sdr_hpsdr/trunk/W5WC/PowerSDR_HPSDR_mRX/Source/DttSP/) | 321d 19h | 3049
[W5WC/PowerSDR_HPSDR_2/Source/DttSP](http://svn.tapr.org/repos_sdr_hpsdr/trunk/W5WC/PowerSDR_HPSDR_2/Source/DttSP/) | 553d 21h | 2564
[KV0S/Qt-Radio-Programs/DttSP](http://svn.tapr.org/repos_sdr_hpsdr/trunk/KV0S/Qt-Radio-Programs/DttSP/) | 50d 3h | 3209
[N6LYT/ghpsdr3/trunk/src/DttSP](http://svn.tapr.org/repos_sdr_hpsdr/trunk/N6LYT/ghpsdr3/trunk/src/DttSP/) | 647d 0h | 2421
[N6LYT/Qt/trunk/src/DttSP](http://svn.tapr.org/repos_sdr_hpsdr/trunk/N6LYT/Qt/trunk/src/DttSP/) | 754d 23h | 2322